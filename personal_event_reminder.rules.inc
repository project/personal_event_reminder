<?php
/**
 * @file
 * Creates Rules action used for sending reminder emails
 */

/**
 * Implements hook_rules_event_info().
 * @ingroup rules
 */
function personal_event_reminder_rules_event_info() {
  return array(
    'personal_event_reminders_to_be_sent' => array(
      'label' => t('Event Reminders To Be Sent'),
      'module' => 'personal_event_reminder',
      'group' => 'Personal event reminder',
      'variables' => array(
        'event_owner' => array('type' => 'user', 'label' => t('The user who set the event.')),
        'event_name' => array('type' => 'text', 'label' => t('The name of the event.')),
        'event_date' => array('type' => 'date', 'label' => t('The date of the event.')),
      ),
    ),
  );
}

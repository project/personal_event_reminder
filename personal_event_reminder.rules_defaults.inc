<?php
/**
 * @file
 * personal_event_reminder.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function personal_event_reminder_default_rules_configuration() {
  $items = array();
  $items['rules_per_send_event_reminder_email'] = entity_import('rules_config', '{ "rules_per_send_event_reminder_email" : {
      "LABEL" : "Send Event Reminder Email",
      "PLUGIN" : "reaction rule",
      "REQUIRES" : [ "rules", "personal_event_reminder" ],
      "ON" : [ "personal_event_reminders_to_be_sent" ],
      "DO" : [
        { "mail" : {
            "to" : "[event-owner:mail]",
            "subject" : "Upcoming Event - [event-name:safe]",
            "message" : "You asked to be reminded about \\u0027[event-name:safe]\\u0027 which is coming up on [event-date:custom:F] [event-date:custom:j].",
            "from" : "[site:mail]",
            "language" : [ "" ]
          }
        }
      ]
    }
  }');
  return $items;
}

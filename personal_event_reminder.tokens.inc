<?php
/**
 * @file
 * Provides tokens for entity properties which have no token yet.
 */

/**
 * Implements hook_token_info().
 */
function personal_event_reminder_token_info() {
  $info['types']['text'] = array(
    'name' => t('Text'),
    'description' => t('Any text.'),
  );
  $info['tokens']['text']['safe'] = array(
    'name' => t('Safe'),
    'description' => t('The sanitized value of the text.'),
  );

  $info['tokens']['text']['raw'] = array(
    'name' => t('Raw'),
    'description' => t('The raw value of the text.'),
  );

  return $info;
}

/**
 * Implements hook_tokens().
 */
function personal_event_reminder_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $replacements = array();

  if ($type == 'text') {
    if (!empty($data['text'])) {
      $text = $data['text'];

      foreach ($tokens as $name => $original) {
        switch ($name) {
          case 'raw':
            $replacements[$original] = $text;
            break;

          case 'safe':
            $replacements[$original] = check_plain($text);
            break;

        }
      }
    }

  }

  return $replacements;
}

DESCRIPTION
-----------
Adds an event reminder field collection to the user profile page. This allows
users to give a name/date combination for events they wish to be emailed
reminders about. This module is not necessarily intended as a calendar type
tool but rather is intended for e-commerce or related sites so that their
users can be reminded of events that they will likely need to purchase items
for. The notification interval is set at a site level rather than a user level
so that the site owner can be sure that the notification goes out with enough
lead time so that their customers can order products for the indicated events.

On the admin side, a form is created at admin/config/people/per that allows a
site wide variable for how many days ahead of the events users should be
notified. The emails are sent via a rule so that the body of the email can be
altered as needed. Also, a field is added as part of the field collection that
is only visible to admins which indicates whether or not a notification has
already been set. If this box is unchecked it could lead to a duplicate email
being sent.

Please note that the sending of emails is tied to cron execution.

AUTHOR
------
Dave Pullen AKA AngryWookie

FEATURE WISH LIST
-----------------
- Better error catching on flagging sent notifications in the case where an
admin might add extra conditions to the rule.
- Allow users to specify their own reminder interval that will work in
addition to the site wide panel.
